#!/usr/bin/perl

use strict;
use warnings;

use Dpkg ();
use Dpkg::Control;
use Dpkg::Control::Info;
use Dpkg::Substvars;

my $substvars = Dpkg::Substvars->new();

while (@ARGV) {
	$_ = shift(@ARGV);
	if (m/^-V(\w[-:0-9A-Za-z]*)[=:]/p) {
	        $substvars->set_as_used($1, ${^POSTMATCH});
	} elsif (m/^--$/p) {
		last;
	}
}

my $controlfile = $ARGV[0];
my $control = Dpkg::Control::Info->new($controlfile);

print "# NOTE: This file is generated from debian/control.in. To regenerate,\n";
print "# run \`make -f debian/rules debian/control'.\n";

my $src_fields = $control->get_source();
$src_fields->{'Source'} = $substvars->substvars($src_fields->{'Source'});
$src_fields->output(\*STDOUT);

foreach my $pkg_fields ($control->get_packages()) {
	print "\n";
	$pkg_fields->{'Package'} = $substvars->substvars($pkg_fields->{'Package'});
	$pkg_fields->output(\*STDOUT);
}
